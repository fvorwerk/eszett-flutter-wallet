import 'package:flutter/material.dart';
import './Details.dart' as first;
import './Receive.dart' as second;
import './Send.dart' as third;
import './Settings.dart' as fourth;

void main() {
  runApp(new MaterialApp(
      home: new MyTabs()
  ));
}

class MyTabs extends StatefulWidget {
  @override
  MyTabsState createState() => new MyTabsState();
}

class MyTabsState extends State<MyTabs> with SingleTickerProviderStateMixin {


  TabController controller;

  @override
  void initState() {
    super.initState();
    controller = new TabController(vsync: this, length: 4);
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {


    return new Scaffold(
        appBar: new AppBar(
            title: new Text("EsZett Wallet"),
            backgroundColor: Colors.black,

        ),
        bottomNavigationBar: new Material(
            color: Colors.black,
            child: new TabBar(
                controller: controller,
                tabs: <Tab>[
                  new Tab(icon: new Icon(Icons.list)),
                  new Tab(icon: new Icon(Icons.arrow_upward)),
                  new Tab(icon: new Icon(Icons.arrow_downward)),
                  new Tab(icon: new Icon(Icons.settings)),

                ]
            )
        ),
        body: new TabBarView(
            controller: controller,
            children: <Widget>[
              new first.Details(),
              new second.Receive(),
              new third.Send(),
              new fourth.Settings()
            ]
        )
    );

  }

}